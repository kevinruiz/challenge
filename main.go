package main

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"math"
	"net/http"
	"os"
	"strings"

	_ "github.com/go-sql-driver/mysql"
	"github.com/julienschmidt/httprouter"
	"github.com/rs/cors"
	"google.golang.org/appengine"
)

var usuarioBD, passBD, hostBD, portBD, nombreBD, conBD, port string
var satelitesTri []Satelite // Donde se guardar los satelites que van a ser usados

//Funcion Principal
func main() {
	usuarioBD = os.Getenv("USUARIO_BD")
	passBD = os.Getenv("PASS_BD")
	hostBD = os.Getenv("HOST_BD")
	portBD = os.Getenv("PORT_BD")
	nombreBD = os.Getenv("NOMBRE_BD")
	port = os.Getenv("PORT")
	conBD = os.Getenv("CONEXION")
	//Endpoint
	router := httprouter.New()
	//fs := http.FileServer(http.Dir("./documentacion/swagger"))
	router.ServeFiles("/api-docs/*filepath", http.Dir("./documentacion/swagger"))
	router.POST("/topsecret", topsecret)
	router.POST("/topsecret_split/:satelite", topsecret_split)
	router.GET("/topsecret_split/", topsecret_split)
	c := cors.New(cors.Options{
		AllowedMethods: []string{"GET", "POST"},
	}).Handler(router)
	if err := http.ListenAndServe(":"+port, c); err != nil {
		log.Fatal(err)
	}
}

//Funcion para crear la conexion a la base de datos
func conexionBD() (db *sql.DB) {
	var data string
	driver := "mysql"
	if appengine.IsDevAppServer() {
		data = fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", usuarioBD, passBD, hostBD, portBD, nombreBD)
	} else {
		data = fmt.Sprintf("%s:%s@unix(/cloudsql/%s)/%s", usuarioBD, passBD, conBD, nombreBD)
	}
	db, err := sql.Open(driver, data)
	if err != nil {
		log.Println(err.Error())
		panic(err.Error())
	}
	return db
}

//Modelos de datos
type BodySatelites struct {
	Satelite []Satelite `json:"satellites"`
}
type BodySatelite struct {
	Distancia float32  `json:"distance"`
	Mensaje   []string `json:"message"`
}
type Satelite struct {
	ID              int
	Nombre          string   `json:"name"`
	Mensaje         []string `json:"message"`
	Distancia       float32  `json:"distance"`
	Pos_x           float32
	Pos_y           float32
	Descripcion     sql.NullString
	LogDateCreated  sql.NullString
	LogDateModified sql.NullString
	Active          sql.NullBool
}
type Response struct {
	Position Posicion `json:"position"`
	Message  string   `json:"message"`
}
type Posicion struct {
	X float32 `json:"x"`
	Y float32 `json:"y"`
}
type Registro struct {
	ID              int
	Satelite_fk     int
	Distancia       float32 `json:"distance"`
	Mensaje         string
	MensajeArray    []string `json:"message"`
	LogDateCreated  sql.NullString
	LogDateModified sql.NullString
	Active          sql.NullBool
}

//input: los 2 satelites y sus 2 radios
//output: los puntos de interseccion de las circunferencias
func interseccion(a Satelite, b Satelite, da float32, db float32) (p []float32) {
	dx, dy := float64(b.Pos_x-a.Pos_x), float64(b.Pos_y-a.Pos_y)
	lr := float64(da + db)                    // suma del radio
	dr := math.Abs(float64(da) - float64(db)) // Diferencia de radio
	ab := math.Sqrt(float64(dx*dx + dy*dy))   // distancia al centro
	if ab <= lr && ab > dr {
		theta1 := math.Atan(dy / dx)
		ef := lr - ab
		ao := float64(da) - ef/2
		theta2 := math.Acos(ao / float64(da))
		theta := theta1 + theta2
		xc := float64(a.Pos_x) + float64(da)*math.Cos(theta)
		yc := float64(a.Pos_y) + float64(da)*math.Sin(theta)
		p = append(p, float32(xc), float32(yc))
		if ab < lr { // Dos intersecciones
			theta3 := math.Acos(ao / float64(da))
			theta = theta3 - theta1
			xd := float64(a.Pos_x) + float64(da)*math.Cos(theta)
			yd := float64(a.Pos_y) - float64(da)*math.Sin(theta)
			p = append(p, float32(xd), float32(yd))
		}
	}
	return
}

// input: distancia al emisor tal cual se recibe en cada satélite
// output: las coordenadas ‘x’ e ‘y’ del emisor del mensaje
func GetLocation(distances ...float32) (x, y float32, err error) {
	var interseccionAB, interseccionAC, interseccionBC []float32
	var xp, yp float32
	hayInterseccion1, hayInterseccion2 := false, false
	if len(distances) < 3 {
		return 0, 0, errors.New("no hay 3 distancia para calcular")
	}
	if satelitesTri[0].Pos_x < satelitesTri[1].Pos_x {
		interseccionAB = interseccion(satelitesTri[0], satelitesTri[1], distances[0], distances[1])
	} else {
		interseccionAB = interseccion(satelitesTri[1], satelitesTri[0], distances[1], distances[0])
	}
	if satelitesTri[0].Pos_x < satelitesTri[2].Pos_x {
		interseccionAC = interseccion(satelitesTri[0], satelitesTri[2], distances[0], distances[2])
	} else {
		interseccionAC = interseccion(satelitesTri[2], satelitesTri[0], distances[2], distances[0])
	}
	if satelitesTri[1].Pos_x < satelitesTri[2].Pos_x {
		interseccionBC = interseccion(satelitesTri[1], satelitesTri[2], distances[1], distances[2])
	} else {
		interseccionBC = interseccion(satelitesTri[2], satelitesTri[1], distances[2], distances[1])
	}

	for i := 0; i < len(interseccionAB); i = i + 2 {
		for a := 0; a < len(interseccionAC); a = a + 2 {
			if interseccionAB[i] == interseccionAC[a] && interseccionAB[i+1] == interseccionAC[a+1] {
				xp = interseccionAB[i]
				yp = interseccionAB[i+1]
				hayInterseccion1 = true
			}
		}
	}
	if !hayInterseccion1 {
		return 0, 0, errors.New("no hay interseccion entre los circulos")
	}
	for a := 0; a < len(interseccionBC); a = a + 2 {
		if interseccionBC[a] == xp && interseccionBC[a+1] == yp {
			hayInterseccion2 = true
		}
	}
	if !hayInterseccion2 {
		return 0, 0, errors.New("no hay interseccion entre los circulos")
	}
	return xp, yp, nil
}

// input: el mensaje tal cual es recibido en cada satélite
// output: el mensaje tal cual lo genera el emisor del mensaje
func GetMessage(messages ...[]string) (msg string, err error) {
	var mensajeFinal string
	var mensaje []string
	/*Cantidad de mensaje que hay*/
	for i := 0; i < len(messages); i++ {
		/*Cantidad de strings que hay*/
		for a := 0; a < len(messages[i]); a++ {
			//Al iniciar guardo todo el mensaje
			if i == 0 {
				mensaje = append(mensaje, messages[i][a])
			} else {
				//Pregunto si el mensaje nuevo no es igual a ""
				if !strings.EqualFold(messages[i][a], "") {
					//Pregunto si el mensaje actual es igual a ""
					if strings.EqualFold(mensaje[a], "") {
						//Actualizo con el nuevo mensaje
						mensaje[a] = messages[i][a]
					}
				}
			}
		}
	}
	//Recorro el mensajeFinal para saber si se pudo codificar el mensaje
	for i := 0; i < len(mensaje); i++ {
		if !strings.EqualFold(mensaje[i], "") {
			if i == 0 {
				mensajeFinal = mensaje[i]
			} else {
				mensajeFinal = mensajeFinal + " " + mensaje[i]
			}
		} else {
			return mensajeFinal, errors.New("no se pudo codificar el mensaje")
		}
	}
	return mensajeFinal, nil
}

//input: nombre del satelite que se va a buscar en base de datos
//output: datos del satelite con
func getSatelite(nombre string) (satelite Satelite, err error) {
	db := conexionBD()
	err = db.QueryRow("SELECT * FROM satelites WHERE NOMBRE=?", nombre).Scan(&satelite.ID, &satelite.Nombre, &satelite.Pos_x, &satelite.Pos_y, &satelite.LogDateCreated, &satelite.LogDateModified, &satelite.Descripcion, &satelite.Active)
	defer db.Close()
	switch {
	case err == sql.ErrNoRows:
		log.Println("Error - no se encontro un satelite con el nombre: ", nombre)
		return satelite, err
	case err != nil:
		log.Println("Error - no se encontro un satelite con el nombre: ", err)
		return satelite, err
	default:
		return satelite, nil
	}
}

//output: datos de todos los satelites
func getSatelites() (satelites []Satelite, err error) {
	db := conexionBD()
	rows, err := db.Query("SELECT * FROM satelites")
	defer db.Close()
	switch {
	case err == sql.ErrNoRows:
		log.Println("Error - no se encontro registros de satelites")
		return satelites, err
	case err != nil:
		return satelites, err
	case err == nil:
		var s []Satelite
		for rows.Next() {
			var satelite Satelite
			err := rows.Scan(&satelite.ID, &satelite.Nombre, &satelite.Pos_x, &satelite.Pos_y, &satelite.LogDateCreated, &satelite.LogDateModified, &satelite.Descripcion, &satelite.Active)
			if err != nil {
				return satelites, err
			}
			s = append(s, satelite)
		}
		return s, err
	default:
		return satelites, nil
	}
}

//input: nombre del satelite que se va a buscar en base de datos
//output: datos del satelite con
func insertRegistro(registro Registro) (e error) {
	var reg Registro
	db := conexionBD()
	err := db.QueryRow("SELECT * FROM registros WHERE Satelite_fk=?", registro.Satelite_fk).Scan(&reg.ID, &reg.Satelite_fk, &reg.Distancia, &reg.Mensaje, &reg.LogDateCreated, &reg.LogDateModified, &reg.Active)
	switch {
	case err == sql.ErrNoRows:
		log.Println("Info - no se encontro un registro con el Satelite_fk: ", registro.Satelite_fk)
		insertData, err := db.Prepare("INSERT INTO registros (Satelite_fk, Distancia, Mensaje, Active ) VALUES (?,?,?,?)")
		if err != nil {
			return err
		}
		_, er := insertData.Exec(registro.Satelite_fk, registro.Distancia, strings.Join(registro.MensajeArray, " "), 1)
		if er != nil {
			log.Println(err)
			panic(er.Error())
		}
		defer db.Close()
		return nil
	case err == nil:
		log.Println("Info - se va a actualizar el registro con el ID: ", reg.ID)
		insertData, err := db.Prepare("UPDATE registros SET Distancia=?, Mensaje=? WHERE ID=?")
		if err != nil {
			return err
		}
		_, er := insertData.Exec(registro.Distancia, strings.Join(registro.MensajeArray, " "), reg.ID)
		if er != nil {
			log.Println(err)
			panic(er.Error())
		}
		defer db.Close()
		return nil
	case err != nil:
		defer db.Close()
		log.Println("Error - ", err)
		return err
	default:
		defer db.Close()
		return nil
	}
}
func getRegistro(satelite Satelite) (registro Registro, err error) {
	var reg Registro
	db := conexionBD()
	rows, err := db.Query("SELECT * FROM registros WHERE Satelite_fk=?", satelite.ID)
	defer db.Close()
	switch {
	case err == sql.ErrNoRows:
		log.Println("Info - no se encontro un registro con el Satelite_fk: ", registro.Satelite_fk)
		return registro, err
	case err == nil:
		for rows.Next() {
			err := rows.Scan(&reg.ID, &reg.Satelite_fk, &reg.Distancia, &reg.Mensaje, &reg.LogDateCreated, &reg.LogDateModified, &reg.Active)
			if err != nil {
				return registro, err
			}
		}
		return reg, nil
	case err != nil:
		log.Println("Error - ", err)
		return registro, err
	default:
		return registro, err
	}
}

//Funcion ruta /topsecret
func topsecret(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	satelitesTri = []Satelite{}
	log.Println("--------------------------------------------------")
	log.Println("Info - URL: ", r.URL)
	log.Println("Info - Method: ", r.Method)
	switch r.Method {
	case http.MethodPost:
		var body BodySatelites
		var mensaje [][]string
		var distancia []float32
		var response = Response{}
		err := json.NewDecoder(r.Body).Decode(&body)
		if err != nil {
			log.Println("Error - ", err.Error())
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		for i := 0; i < len(body.Satelite); i++ {
			satelite, err := getSatelite(body.Satelite[i].Nombre)
			if err != nil {
				log.Println("Error - ", err.Error())
				http.Error(w, "RESPONSE CODE: 404", http.StatusNotFound)
				return
			}
			satelitesTri = append(satelitesTri, satelite)
			distancia = append(distancia, body.Satelite[i].Distancia)
			mensaje = append(mensaje, body.Satelite[i].Mensaje)
		}
		response.Message, err = GetMessage(mensaje...)
		if err != nil {
			log.Println("Error - ", err.Error())
			http.Error(w, "RESPONSE CODE: 404", http.StatusNotFound)
			return
		}
		response.Position.X, response.Position.Y, err = GetLocation(distancia...)
		if err != nil {
			log.Println("Error - ", err.Error())
			http.Error(w, "RESPONSE CODE: 404", http.StatusNotFound)
			return
		}
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusOK)
		if err := json.NewEncoder(w).Encode(response); err != nil {
			log.Println("Error - ", err.Error())
			panic(err)
		} else {
			log.Println("Info - Status: OK")
		}
	default:
		log.Println("Error - Method not allowed")
		http.Error(w, "RESPONSE CODE: 404", http.StatusNotFound)
	}
}

//Funcion ruta /topsecret_split
func topsecret_split(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	satelitesTri = []Satelite{}
	log.Println("--------------------------------------------------")
	log.Println("Info - URL: ", r.URL)
	log.Println("Info - Method: ", r.Method)
	data := p.ByName("satelite")
	//var response Response
	switch r.Method {
	case http.MethodPost:
		var registro Registro
		satelite, err := getSatelite(data)
		if err != nil {
			log.Println("Error - ", err.Error())
			http.Error(w, "RESPONSE CODE: 404", http.StatusNotFound)
			return
		}
		registro.Satelite_fk = satelite.ID
		er := json.NewDecoder(r.Body).Decode(&registro)
		if er != nil {
			log.Println("Error - ", er.Error())
			http.Error(w, er.Error(), http.StatusBadRequest)
			return
		}
		e := insertRegistro(registro)
		if e != nil {
			log.Println("Error - ", e.Error())
			http.Error(w, "RESPONSE CODE: 404", http.StatusNotFound)
			return
		}
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusOK)

	case http.MethodGet:
		var response Response
		var mensajes [][]string
		var distancias []float32
		ss, err := getSatelites()
		if err != nil {
			log.Println("Error - ", err.Error())
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		satelitesTri = ss
		for i := 0; i < len(ss); i++ {
			registro, err := getRegistro(ss[i])
			if err != nil {
				log.Println("Error - ", err.Error())
				http.Error(w, err.Error(), http.StatusBadRequest)
				return
			}
			MensajeArray := strings.Split(registro.Mensaje, " ")
			mensajes = append(mensajes, MensajeArray)
			distancias = append(distancias, registro.Distancia)
		}
		response.Message, err = GetMessage(mensajes...)
		if err != nil {
			log.Println("Error - ", err.Error())
			http.Error(w, "RESPONSE CODE: 404", http.StatusNotFound)
			return
		}
		response.Position.X, response.Position.Y, err = GetLocation(distancias...)
		if err != nil {
			log.Println("Error - ", err.Error())
			http.Error(w, "RESPONSE CODE: 404", http.StatusNotFound)
			return
		}
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusOK)
		if err := json.NewEncoder(w).Encode(response); err != nil {
			log.Println("Error - ", err.Error())
			panic(err)
		} else {
			log.Println("Info - Status: OK")
		}
	default:
		log.Println("Error - Method not allowed")
		http.Error(w, "RESPONSE CODE: 404", http.StatusNotFound)
	}
}
