-- challenge.satelites definition

CREATE TABLE IF NOT EXISTS `satelites` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(100) NOT NULL,
  `Pos_x` float NOT NULL,
  `Pos_y` float NOT NULL,
  `Descripcion` varchar(100) DEFAULT NULL,
  `LogDateCreated` datetime DEFAULT NULL,
  `LogDateModified` datetime DEFAULT NULL,
  `Active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- insercion de datos

INSERT INTO satelites
(Nombre, Pos_x, Pos_y, Descripcion, LogDateCreated, LogDateModified, Active)
VALUES('Kenobi', -500, -200, '', null, null, 1);
INSERT INTO satelites
(Nombre, Pos_x, Pos_y, Descripcion, LogDateCreated, LogDateModified, Active)
VALUES('Skywalker', 100, -100, '', null, null, 1);
INSERT INTO satelites
(Nombre, Pos_x, Pos_y, Descripcion, LogDateCreated, LogDateModified, Active)
VALUES('Sato', 500, 100, '', null, null, 1);

-- challenge.registros definition

CREATE TABLE `registros` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Satelite_fk` int(10) unsigned NOT NULL,
  `Distancia` float NOT NULL,
  `Mensaje` varchar(100) NOT NULL,
  `LogDateCreated` datetime DEFAULT NULL,
  `LogDateModified` datetime DEFAULT NULL,
  `Active` tinyint(1) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `registros_FK` (`Satelite_fk`),
  CONSTRAINT `registros_FK` FOREIGN KEY (`Satelite_fk`) REFERENCES `satelites` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
