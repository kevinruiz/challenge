module gitlab.com/kevinruiz/challenge

go 1.16

require (
	github.com/go-sql-driver/mysql v1.6.0
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/julienschmidt/httprouter v1.3.0
	github.com/rs/cors v1.7.0
	golang.org/x/net v0.0.0-20210525063256-abc453219eb5 // indirect
	google.golang.org/appengine v1.6.7
)
