## Instalacion APP

1. Clonar el proyecto: `git clone https://gitlab.com/kevinruiz/challenge.git`
2. Ir a la carpeta del repo: `cd "carpeta"`

### Servidor MySQL 8

2. Ejecutar: `mysql -u root -p` e ingresar contraseña cuando lo pida
3. Ejecutar: `CREATE DATABASE "nombre_db"`
4. Ejecutar: `USE "nombre_db"`
5. Ejecutar el script del repo ubicado en la carpeta /script: `SOURCE script.sql;`

### Entorno local

6. Desde console ir al directorio donde se clono el proyecto
7. Ejecutar: `dev_appserver.py app.yaml`

### Deploy productivo

6. Desde consola ir al directorio donde se clono el proyecto
7. Ejecutar: `gcloud app deploy`

### Variables de entorno del proyecto 

Estas se guardar en un archivo app.yaml para poder utilizar en el proyecto.

    - USUARIO_BD: Usuario para conectarse al servidor base de datos
    - PASS_BD: Contraseña para conectarse al servidor de base de datos
    - HOST_BD: IP o dominio donde esta ubicado el servidor MySQL
    - PORT_BD: Puerto para conectarse a la base de datos
    - NOMBRE_BD: Nombre de la base de datos
    - PORT: Puerto donde se va a utlizar la API
    - CONEXION: Nombre de la instancia 


