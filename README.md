# Challenge MeLi Backend

Este proyecto esta enfocado a solucionar el challenge de backend encomendado por MeLi.

## Tecnologias
    - Google App Engine. Instalacion: https://cloud.google.com/sdk/docs/install
    - Golang 1.12+. Instalacion: https://golang.org/dl/
    - MySQL 8.0

## URL PRD
    - https://challengemeli-314020.rj.r.appspot.com/

## Documentacion API REST
    - https://challengemeli-314020.rj.r.appspot.com/api-docs/





